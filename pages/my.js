require("mocha-allure-reporter");
const {expect} = require("chai");
const assert = require("assert");

module.exports = function first(param) {
  this.selectPage = allure.createStep("first", (name) => {
    allure.createAttachment(param, "attach" + name);
  })
}


function second(param) {
  allure.createStep("second", (name) => {
    allure.createAttachment(name, "attach" + name);
  })
}

function Ppp() {
  this.readme = "readme";
  this.fileContent = ".file";

  this.selectFile = allure.createStep("select file '{0}'", name => {
    allure.createAttachment(this.readme, "attach" + name);
  });
}

function step1() {
  allure.createStep('step1', function () {
    expect(false).to.equal(true);
    return allure.createAttachment("2", "attach"); // if promise returned, we will wait this before finish the step
  })();
  assert.strictEqual(1, 7, "sdfhlsdfhsd")
  allure.createAttachment("1", "attach"); // if promise returned, we will wait this before finish the step
}

const stepToBreak2 = allure.createStep("break test 2", () => {
  allure.createAttachment("sdsds2", "ksj;aflkjas;flkjasd");
  expect(23).to.equal(true);
  //throw new Error("Make test broken");
});

module.exports = {
  Ppp, step1, stepToBreak2
};